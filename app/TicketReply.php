<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketReply extends Model
{
    //
    protected $fillable = [
        'referenceNo',
        'userId',
        'cusId'        
    ];

    public function customers(){
        return $this->belongTo('App/Customer');
    }

    public function users(){
        return $this->belongTo('App/User');
    }

    public function tickets(){
        return $this->belongTo('App/Ticket');
    }
}
