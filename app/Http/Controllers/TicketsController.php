<?php

namespace App\Http\Controllers;

use App\Ticket;
use Illuminate\Http\Request;

use App\Mail\ReplyMail;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Auth;

class TicketsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::check()){
        $ticket = Ticket::paginate(2);

        return view('ticket.index',['tickets'=>$ticket]);

    }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('ticket.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $ticket = Ticket::create([
            'cusName' => $request->input('cusname'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'referenceNo' =>$request->input('ref'),
            'problem' =>$request->input('problem'),
            'status' =>$request->input('status')
            
            

        ]);

        $refNo = $request->input('ref');

        Mail::to($ticket->email)->send(new TicketMail($refNo));

        if($ticket){
            return redirect()->route('ticket.create')->with('sucess','Successfully Send the Ticket');
        }

        return back()->withInput()->with('errors','Error Adding new ticket');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        //
        $ticket = Ticket::find($ticket->id);

        return view('ticket.show',['ticket'=>$ticket]);
    }


    public function refshow(Ticket $ticket)
    {
        //
        $ticket = Ticket::find($ticket->referenceNo);

        return view('ticket.refshow',['ticket'=>$ticket]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
        $ticket = Ticket::find($ticket->id);

        return view('ticket.edit',['ticket'=>$ticket]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        //

        $ticketUpdate = Ticket::where('id', $ticket->id)
            ->update([
                'reply'=>$request->input('reply'),
                'status'=>$request->input('status')              

            ]); 
            

            $reply = $request->input('reply');
            Mail::to($ticket->email)->send(new ReplyMail($reply));
           

        if($ticketUpdate){
            return redirect()->route('ticket.show',['ticket'=> $ticket->id])
            ->with('sucess','Successfully Send the reply');
        }

        

        return back()->withInput();

        
        
        

    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }
}
