<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    //
    protected $fillable = [
        'referenceNo',
        'cusName',
        'email',
        'phone',
        'problem',
        'reply',
        'status'
            
    ];

    public function ticketsReply(){
        return $this->belongTo('App/TicketReply');
    }

    public function customers(){
        return $this->belongTo('App/Customer');
    }

    public function users(){
        return $this->belongTo('App/User');
    }
}
