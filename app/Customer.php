<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $fillable = [
        'name',
        'email',
        'phone'        
    ];

    public function tickets(){
        return $this->hasMany('App/Ticket');
    }

    public function ticketsReply(){
        return $this->belongsToMany('App/TicketReply');
    }

}
