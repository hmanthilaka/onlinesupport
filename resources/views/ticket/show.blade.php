@extends('layouts/app')

@section('content')

<div class="container mt-5">
    <center><h2>Ticket Details</h2></center>
    
  <div class="form-group">
    <label for="exampleInputEmail1">Reference Number</label>
    <input class="form-control" type="text" id="ref" name="ref" value="{{$ticket->referenceNo}}" readonly>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Customer Name</label>
    <input class="form-control" type="text" id="cusname" name="cusname" value="{{$ticket->cusName}}" readonly>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">E-mail</label>
    <input class="form-control" type="text" id="email" name="email" value="{{$ticket->email}}" readonly>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Phone</label>
    <input class="form-control" type="text" id="phone" name="phone" value="{{$ticket->phone}}" readonly>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Problem</label>
    <textarea class="form-control" id="problem" name="problem" readonly>{{$ticket->problem}}</textarea>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Reply</label>
    <textarea class="form-control" id="reply" name="reply">{{$ticket->reply}}</textarea>
  </div>
  <form action="/ticket">
  <button type="submit" class="btn btn-primary">Back</button>
  </form>

    </div>


@endsection
