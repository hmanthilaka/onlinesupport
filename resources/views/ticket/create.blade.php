@extends('layouts/app')

@section('content')

<div class="container mt-5">
    <center><h2>Ticket Details</h2></center>
    <form method="post" action={{ route('ticket.store') }}">
        {{ csrf_field()}}

    

  <div class="form-group">
    <label for="exampleInputEmail1">Reference Number</label>
    <input class="form-control" type="text" id="ref" name="ref" >
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Customer Name</label>
    <input class="form-control" type="text" id="cusname" name="cusname">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">E-mail</label>
    <input class="form-control" type="text" id="email" name="email">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Phone</label>
    <input class="form-control" type="text" id="phone" name="phone">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Problem</label>
    <textarea class="form-control" id="problem" name="problem"></textarea>
  </div>
  
  <input type="hidden" name="status" value="New">
  <input type="hidden" name="ref" value="{{str_random(10)}}">
  <button type="submit" class="btn btn-primary">Send</button>
</form>
    </div>


@endsection
