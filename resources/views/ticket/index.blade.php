@extends('layouts/app')

@section('content')

<style>
 @media (max-width: 300px) {

.test1 {
    width: 100% !important;
    float: left;
}
}
</style>


<div class="container col-md-9 col-lg-9 col-sm-3">
    <h1>Ticket List</h1>
    <br>

    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for Customer" title="Type in a name">
    </br></br>
                <table class="table table-bordered mb-5" id="myTable">
                    <thead>
                        <tr class="table-success">
                            <th scope="col" class="test1">#</th>
                            <th scope="col" class="test1">Reference No</th>
                            <th scope="col" class="test1">Customer name</th>
                            <th scope="col" class="test1">Status</th>
                            <th scope="col" class="test1">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tickets as $ticket)
                        <tr>
                            <th scope="row" class="test1">{{$ticket->id }}</th>
                            <td class="test1">{{ $ticket->referenceNo }}</td>
                            <td class="test1">{{ $ticket->cusName }}</td>
                            @if( $ticket->status == 'New' )
                            <td class="test1"><font color="red">{{ $ticket->status }}</font></td>
                            @else
                            <td class="test1">{{ $ticket->status }}</td>
                            @endif
                            <td class="test1"><a href="/ticket/{{ $ticket->id }}" ><button class='btn btn-success btn-sm'>View</button></a><a href="/ticket/{{ $ticket->id }}/edit" >&nbsp;<button class='btn btn-success btn-sm'>Reply</button></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $tickets->links() }}
    </div>


    <script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

                </div>

@endsection
