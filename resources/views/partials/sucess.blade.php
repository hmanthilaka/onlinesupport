@extends('layouts/app')

@section('content')

    @if (session()->has('success'))
    <div class="alert alert-dismissable alert-success">
        <button type="button" class="close" date-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    <strong>
        {!! session()->get(success) !!}
    </strong>
    </div>
@endif


@endsection