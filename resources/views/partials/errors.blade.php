@if (isset($errors)&&count($errors)>0)
    <div class="alert alert-dismissable alert-danger">
        <button type="button" class="close" date-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
@foreach ($errors->all() as $error)
    <li><strong>{!! $error !!}</strong></li>
@endforeach
    </div>
@endif